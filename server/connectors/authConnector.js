import { createToken, checkPwd, generateId } from '../utils/crypto'
import { ANONYMOUS_TOKEN_DATA } from '../utils/consts'

// export const getViewer = (currentUser) => {
//     return currentUser
// }

export const loginAction = (currentUser, Model, args, session) => {
    let lastLoggedIn = ''
    return Model.findOne({
            where: { username: args.username }
        })
        .then((user) => {
            if (!user && !checkPwd(args.password, user.password)) {
                throw new Error('The username and Password don\'t match')
            }
            lastLoggedIn = user.lastLoggedIn
            user.lastLoggedIn = Date()
            return user.save()
                .then((updatedUser) => {
                    updatedUser.lastLoggedIn = lastLoggedIn
                    updatedUser.id = ANONYMOUS_TOKEN_DATA.id
                    session.token = createToken(updatedUser)
                    session.id = generateId()
                    currentUser = updatedUser
                    return updatedUser
                }).catch((err) => {
                    console.log(err)
                    throw new Error('internal server error')
                })
        }).catch((err) => {
            // didn't find the user
            console.log(err)
            throw new Error('The Username and password don\'t match')
        })
}

export const logOutAction = (session) => {
    session = null
    return ANONYMOUS_TOKEN_DATA
}
