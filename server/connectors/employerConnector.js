import { generateId } from '../utils/crypto'
export const getEmployers = (currentUser, Model, args) => {
    // console.log(user.role)
    switch (currentUser.role) {
    case 'admin':
    case 'leader':
    case 'organiser':
    case 'anonymous':
        return Model.findAll()
            .then(eployers => eployers)
            .catch((err) => {
                console.log(JSON.stringify(err))
                throw new Error(err)
            })
    default:
        throw new Error('no employer not found')
            // return null
    }
}

export const createEmployer = (currentUser, Model, args) => {
    if (currentUser.role !== 'admin') {
        throw new Error('You must be admin to create employers')
    }
    args.id = args.id || generateId()
    return Model.create(args)
        .then(employer => employer)
        .catch((err) => {
            console.log(JSON.stringify(err))
            throw new Error(err)
        })
}

export const removeEmployer = (currentUser, Model, args) => {
    // console.log(user.role)
    switch (currentUser.role) {
    case 'admin':
    case 'leader':
    case 'organiser':
    case 'anonymous':
        return Model.findOne({ where: { id: args.id } })
            .then(employer => {
                employer.destroy()
                return true
            })
            .catch(() => {
                // console.log(JSON.stringify(err))
                throw new Error('Employer doesn\'t exist')
            })
    default:
        throw new Error('you dont have permissions')
            // return null
    }
}
