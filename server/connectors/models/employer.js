export default (sequelize, DataTypes) => {
    var Employer = sequelize.define('Employer', {
            id: {
                type: DataTypes.UUID,
                primaryKey: true,
                validate: {
                    isUUID: 4
                }
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false,
                validate: {
                    len: [2, 24]
                }
            },
            street: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    len: [2, 24]
                }
            },
            street2: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    len: [2, 24]
                }
            },
            suburb: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    len: [2, 24]
                }
            },
            state: {
                type: DataTypes.STRING,
                allowNull: false,
                validate: {
                    len: [2, 16]
                }
            },
            postCode: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    len: [2, 4]
                }
            },
            email: {
                type: DataTypes.STRING,
                allowNull: true,
                unique: true,
                validate: { isEmail: true }
            },
            phone: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    len: [6, 10]
                }
            },
            fax: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    len: [6, 10]
                }
            }
        }, {
            // don't delete database entries but set the newly added attribute deletedAt
            // to the current date (when deletion was done). paranoid will only work if
            // timestamps are enabled
            paranoid: true,
            classMethods: {
                associate: function (models) {
                    Employer.hasMany(models.Site)
                }
            }
        })
    return Employer
}
