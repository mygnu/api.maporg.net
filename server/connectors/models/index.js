import fs from 'fs'
import path from 'path'
import Sequelize from 'sequelize'
import { dbConfig } from '../../utils/config'
import mockData from '../../utils/mockData'

const env = process.env.NODE_ENV || 'development'
const config = dbConfig[env]
const sequelize = new Sequelize(config.database, config.username, config.password, config)

// if (process.env.DATABASE_URL) {
//     sequelize = new Sequelize(process.env.DATABASE_URL)
// } else {
//     sequelize = new Sequelize(config.database, config.username, config.password, config)
// }
var db = {}

fs.readdirSync(__dirname)
    .filter(function (file) {
        return (file.indexOf('.') !== 0) && (file !== 'index.js')
    })
    .forEach(function (file) {
        var model = sequelize.import(path.join(__dirname, file))
        db[model.name] = model
            // model.sync()
    })

Object.keys(db).forEach(function (modelName) {
    if ('associate' in db[modelName]) {
        db[modelName].associate(db)
    }
})
setTimeout(() => {
    if (process.env.NODE_ENV !== 'production') {
        sequelize.sync({ force: true })
        setTimeout(() => {
            mockData(db)
        }, 3000)
    } else {
        sequelize.sync()
    }
}, 1000)

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
