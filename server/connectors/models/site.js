export default (sequelize, DataTypes) => {
    var Site = sequelize.define('Site', {
            id: {
                type: DataTypes.UUID,
                primaryKey: true,
                validate: {
                    isUUID: 4
                }
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false,
                validate: {
                    len: [2, 24]
                }
            },
            street: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    len: [2, 24]
                }
            },
            street2: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    len: [2, 24]
                }
            },
            suburb: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    len: [2, 24]
                }
            },
            state: {
                type: DataTypes.STRING,
                allowNull: false,
                validate: {
                    len: [2, 16]
                }
            },
            postCode: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    len: [2, 4]
                }
            },
            email: {
                type: DataTypes.STRING,
                allowNull: true,
                unique: true,
                validate: { isEmail: true }
            },
            phone: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    len: [6, 10]
                }
            },
            fax: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    len: [6, 10]
                }
            }
        }, {
            paranoid: true,
            classMethods: {
                associate: function (models) {
                    // Site.belongsTo(models.Employer)
                    Site.hasMany(models.Worker)
                    Site.hasMany(models.Task)
                }
            }
        })
        // Site.sync()
        // Site.create({
        //         username: 'fnord1',
        //         createdBy: 'admin',
        //         firstName: 'Harry',
        //         lastName: 'Gill',
        //         role: 'admin',
        //         email: 'test1@test.com',
        //         password: hashPwd('password')
        //     })
        //     .then(function (res) {
        //         console.log(res)
        // Site
        //     .findOrCreate({ where: { username: 'fnord' }, defaults: { job: 'something else' } })
        //     .spread(function (user, created) {
        //         console.log(user.get({
        //             plain: true
        //         }))
        //         console.log(created)
        //
        //         /*
        //           {
        //             username: 'fnord',
        //             job: 'omnomnom',
        //             id: 2,
        //             createdAt: Fri Mar 22 2013 21: 28: 34 GMT + 0100(CET),
        //             updatedAt: Fri Mar 22 2013 21: 28: 34 GMT + 0100(CET)
        //           }
        //           created: false
        //         */
        //     })
        // }).catch((err) => {console.log(err.errors)})
    return Site
}
