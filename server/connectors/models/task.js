export default (sequelize, DataTypes) => {
    const Task = sequelize.define('Task', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            validate: {
                isUUID: 4
            }
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [4, 25]
            }
        },
        assignedBy: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isUUID: 4
            }
        },
        assignedTo: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isUUID: 4
            }
        },
        startDate: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            validate: {
                isDate: true
            }
        },
        finishDate: {
            type: DataTypes.DATE,
            validate: {
                isDate: true
            }
        },
        memberName: {
            type: DataTypes.STRING,
            allowNull: true,
            validate: {
                len: [4, 25]
            }
        },
        message: { type: DataTypes.STRING, allowNull: true },
        outcome: { type: DataTypes.STRING, allowNull: true },
        status: {
            type: DataTypes.ENUM,
            allowNull: false,
            defaultValue: 'pending',
            values: ['pending', 'done', 'in-progress', 'abandoned', 'overdue'],
            validate: {
                isIn: [
                    ['pending', 'done', 'in-progress', 'abandoned', 'overdue']
                ]
            }
        },
        type: { type: DataTypes.STRING, defaultValue: 'Signup Conversation' }
    }, {
        paranoid: true,
        classMethods: {
            // associate: function (models) {
            // }
        }
    })
    return Task
}
