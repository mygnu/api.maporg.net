import { ROLES } from '../../utils/consts'
import { hashPwd } from '../../utils/crypto'

// create enum for role validation
const roleEnum = []
for (const key of Object.keys(ROLES)) {
    roleEnum.push(ROLES[key])
}

export default (sequelize, DataTypes) => {
    var User = sequelize.define('User', {
        username: {
            type: DataTypes.STRING,
            primaryKey: true,
            validate: {
                len: [5, 10]
            }
        },
        createdBy: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [5, 10]
            }
        },
        password: { type: DataTypes.STRING, allowNull: false },
        firstName: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isAlpha: true,
                len: [2, 12]
            }
        },
        lastName: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isAlpha: true,
                len: [2, 12]
            }
        },
        name: {
            type: DataTypes.STRING,
            get: function () {
                var lastName = this.getDataValue('lastName')
                    // 'this' allows you to access attributes of the instance
                return this.getDataValue('firstName') + ' ' + lastName
            }
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            validate: { isEmail: true }
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: true,
            validate: {
                len: [6, 10]
            }
        },
        role: {
            type: DataTypes.ENUM,
            values: roleEnum,
            allowNull: false,
            validate: {
                isIn: [roleEnum]
            }
        },
        lastLoggedIn: {
            type: DataTypes.STRING,
            allowNull: true,
            validate: {
                isDate: true
            }
        }
    }, {
        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        paranoid: true,
        classMethods: {
            associate: function (models) {
                User.hasMany(models.Site)
            }
        }
    })
    return User
}
