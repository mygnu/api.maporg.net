import { ROLES } from '../../utils/consts'
// import { hashPwd } from '../../utils/crypto'

// create enum for role validation
const roleEnum = []
for (const key of Object.keys(ROLES)) {
    roleEnum.push(ROLES[key])
}

export default (sequelize, DataTypes) => {
    var Worker = sequelize.define('Worker', {
            id: {
                type: DataTypes.UUID,
                primaryKey: true,
                validate: {
                    isUUID: 4
                }
            },
            firstName: {
                type: DataTypes.STRING,
                allowNull: false,
                validate: {
                    isAlpha: true,
                    len: [2, 12]
                }
            },
            lastName: {
                type: DataTypes.STRING,
                allowNull: false,
                validate: {
                    isAlpha: true,
                    len: [2, 12]
                }
            },
            name: {
                type: DataTypes.STRING,
                get: function () {
                    var lastName = this.getDataValue('lastName')
                        // 'this' allows you to access attributes of the instance
                    return this.getDataValue('firstName') + ' ' + lastName
                }
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true,
                validate: { isEmail: true }
            },
            phone: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    len: [6, 10]
                }
            },
            mobile: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    len: [6, 10]
                }
            },
            joinDate: {
                type: DataTypes.DATE,
                allowNull: true,
                validate: {
                    isDate: true
                }
            },
            role: {
                type: DataTypes.ENUM,
                values: ['member', 'non-member', 'leader', 'hostile'],
                allowNull: false,
                defaultValue: 'non-member',
                validate: {
                    isIn: ['member', 'non-member', 'leader', 'hostile']
                }
            }
        }, {
            paranoid: true,
            classMethods: {
                associate: function (models) {
                    // Worker.hasMany(models.Task)
                }
            }
        })
        // Worker.create({
        //         username: 'fnord1',
        //         createdBy: 'admin',
        //         firstName: 'Harry',
        //         lastName: 'Gill',
        //         role: 'admin',
        //         email: 'test1@test.com',
        //         password: hashPwd('password')
        //     })
        //     .then(function (res) {
        //         console.log(res)
        // Worker
        //     .findOrCreate({ where: { username: 'fnord' }, defaults: { job: 'something else' } })
        //     .spread(function (user, created) {
        //         console.log(user.get({
        //             plain: true
        //         }))
        //         console.log(created)
        //
        //         /*
        //           {
        //             username: 'fnord',
        //             job: 'omnomnom',
        //             id: 2,
        //             createdAt: Fri Mar 22 2013 21: 28: 34 GMT + 0100(CET),
        //             updatedAt: Fri Mar 22 2013 21: 28: 34 GMT + 0100(CET)
        //           }
        //           created: false
        //         */
        //     })
        // }).catch((err) => {console.log(err.errors)})
    return Worker
}
