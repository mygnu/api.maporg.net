import { generateId } from '../utils/crypto'
export const getSites = (currentUser, Model, args) => {
    // console.log(user.role)
    switch (currentUser.role) {
    case 'admin':
    case 'leader':
    case 'organiser':
    case 'anonymous':
        return Model.findAll()
            .then(eployers => eployers)
            .catch((err) => {
                console.log(JSON.stringify(err))
                throw new Error(err)
            })
    default:
        throw new Error('no site not found')
            // return null
    }
}

export const createSite = (currentUser, Model, args) => {
    args.id = args.id || generateId()
    return Model.create(args)
        .then(site => site)
        .catch((err) => {
            console.log(JSON.stringify(err))
            throw new Error(err)
        })
}

export const removeSite = (currentUser, Model, args) => {
    // console.log(user.role)
    switch (currentUser.role) {
    case 'admin':
    case 'leader':
    case 'organiser':
    case 'anonymous':
        return Model.findOne({ where: { id: args.id } })
            .then(site => {
                site.destroy()
                return true
            })
            .catch(() => {
                // console.log(JSON.stringify(err))
                throw new Error('Wrong id')
            })
    default:
        throw new Error('you dont have permissions')
            // return null
    }
}
