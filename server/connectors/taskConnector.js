import { generateId } from '../utils/crypto'
export const getTasks = (currentUser, Model, args) => {
    // console.log(user.role)
    switch (currentUser.role) {
    case 'admin':
    case 'leader':
    case 'organiser':
    case 'anonymous':
        return Model.findAll()
            .then(tasks => tasks)

    default:
        throw new Error('task not found')
            // return null
    }
}

export const createTask = (currentUser, Model, args) => {
    if (currentUser.role === 'anonymous') return null

    args.id = args.id || generateId()
    return Model.create(args)
        .then(task => task)
}
