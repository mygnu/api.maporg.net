import { hashPwd, createToken } from '../utils/crypto'

export const getUsers = (currentUser, Model, args) => {
    switch (currentUser.role) {
    case 'admin':
    case 'leader':
    case 'organiser':
    case 'anonymous':
        {
            return Model.findAll()
                .then(users => users)
                .catch((err) => {
                    console.log(JSON.stringify(err))
                })
        }
    default:
        return null
    }
}

export const createUser = (currentUser, Model, args, session) => {
    args.password = hashPwd(args.password)
    return Model.create(args)
        .then(user => {
            if (session) {
                session.token = createToken(user)
            }
            return user
        })
        .catch((err) => {
            console.error(JSON.stringify(err.errors[0]['message']))
            throw new Error(err.errors[0]['message'])
                // response.errors.push(err.name)
                // response.errors.push(err.message)
                // response.success = false
                // return response
        })
}
