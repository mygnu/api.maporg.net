import Mutation from './mutations'
import Query from './query'

const resolveFunctions = {
    Query,
    Mutation,
    Token: {
        __parseValue(value) {
            return value // value from the client
        },
        __serialize(value) {
            return value // value sent to the client
        }
    }

}

export default resolveFunctions
