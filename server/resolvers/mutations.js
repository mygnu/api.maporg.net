import { createUser } from '../connectors/userConnector'
import { createTask } from '../connectors/taskConnector'
// import { createEmployer, removeEmployer } from '../connectors/employerConnector'
import { loginAction, logOutAction } from '../connectors/authConnector'

export default {
    login: (root, args, { models: { User }, currentUser, session }, info) => {
        return loginAction(currentUser, User, args, session)
    },
    logout: (root, args, context, info) => {
        return logOutAction(context.session)
    },
    register: (root, args, { models: { User }, currentUser, session }, info) => {
        return createUser(currentUser, User, args.input, session)
    },
    newUser: (root, args, {models: {User}, currentUser}) => {
        return createUser(currentUser, User, args.input, null)
    },
    newTask: (root, args, { models: { Task }, currentUser }, info) => {
        return createTask(currentUser, Task, args.input)
    }
    // newEmployer: (root, args, user, { rootValue }) => {
    //     return createEmployer(args, user)
    // },
    // deleteEmployer: (root, args, user, { rootValue }) => {
    //     return removeEmployer(args.id, user)
    // }

}
