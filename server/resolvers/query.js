// import { getViewer } from '../connectors/authConnector'
import { getTasks } from '../connectors/taskConnector'
import { getEmployers } from '../connectors/employerConnector'
import { getSites } from '../connectors/siteConnector'
import { getUsers } from '../connectors/userConnector'

export default {
    viewer: (root, args, { models: { User }, currentUser }, info) => {
        // console.log(info)
        return currentUser
    },
    users: (root, args, { models: { User }, currentUser }, info) => {
        // console.log(authorization)
        return getUsers(currentUser, User, args)
    },
    tasks: (root, args, { models: { Task }, currentUser }, info) => {
        // console.log(root)
        return getTasks(currentUser, Task)
    },
    employers: (root, args, { models: { Employer }, currentUser }, info) => {
        // console.log(authorization)
        return getEmployers(currentUser, Employer, args)
    },
    sites: (root, args, { models: { Site }, currentUser }, info) => {
        // console.log(authorization)
        return getSites(currentUser, Site, args)
    }
}
