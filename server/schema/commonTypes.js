export default `


type Address {
    street: String!,
    street2: String,
    suburb: String!,
    state: String!,
    postCode: String!
}

input inputAddress {
    street: String!,
    street2: String,
    suburb: String!,
    state: String!,
    postCode: String!
}

type Contact {
    email: String,
    phone: String,
    fax: String
}

input inputContact {
    email: String,
    phone: String,
    fax: String
}

type CreaetUserResponse {
    success: Boolean!
    errors: [String]!
    user: User
}

type RegisterResponse {
    success: Boolean!
    errors: [String]!
    viewer: Viewer
}

type LoginOutResponse {
    success: Boolean!
    errors: [String]!
    viewer: Viewer
}
scalar Token
`
