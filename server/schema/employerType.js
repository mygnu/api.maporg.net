export default `

type Employer {
    id: ID!,
    name: String!,
    street: String!,
    street2: String,
    suburb: String!,
    state: String!,
    postCode: String!,
    email: String,
    phone: String,
    fax: String
}
input employerInput {
    id: ID!,
    name: String!,
    street: String!,
    street2: String,
    suburb: String!,
    state: String!,
    postCode: String!,
    email: String,
    phone: String,
    fax: String
}
`
