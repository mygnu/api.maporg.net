import commonTypes from './commonTypes'
import employerType from './employerType'
import mutationType from './mutationType'
import queryType from './queryType'
import taskType from './taskType'
import userType from './userType'
import viewerType from './viewerType'
import siteType from './siteType'
import workerType from './workerType'

const schema =
    `
# we need to tell the server which types represent the root query
# and root mutation types. We call them RootQuery and RootMutation by convention.
schema {
    query: Query
    mutation: Mutation
}`

export default [
    schema,
    queryType,
    commonTypes,
    mutationType,
    userType,
    viewerType,
    taskType,
    employerType,
    siteType,
    workerType
]
