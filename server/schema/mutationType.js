export default `
# this schema allows the following mutation:
type Mutation {
    # login by username and password
    login (
        username: String!
        password: String!
    ): Viewer!

    # logout from current session
    logout: Viewer!

    # register new user and login
    register (input: userInput!): Viewer!

    # create new user and return
    newUser (input: userInput!): User!

    # create new user and return
    updateUser (input: userInput!): CreaetUserResponse!


    # create new Task and return
    newTask (input: taskInput!): Task

    # create new user and return
    newEmployer (input: employerInput!): Employer

    deleteEmployer(id: String!): Boolean
}
`
