export default `


# the schema allows the following query:
type Query {
    viewer: Viewer
    tasks: [Task]
    employers: [Employer]
    sites: [Site]
    users: [User]
}
`
