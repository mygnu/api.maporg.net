export default `

# User is someone that can log into the site and can have multiple roles i.e.
# admin, leadOrg, organiser, leader, member
type Task {
    id: ID!,
    title: String!,
    assignedBy: String!,
    assignedTo: String!,
    startDate: String,
    finishDate: String,
    memberName: String,
    message: String,
    outcome: String,
    status: String,
    type: String
}
input taskInput {
    id: String,
    assignedBy: String!,
    assignedTo: String!,
    startDate: String,
    finishDate: String,
    memberName: String,
    message: String,
    outcome: String,
    status: String,
    type: String
}
`
