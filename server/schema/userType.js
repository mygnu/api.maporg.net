export default `
# User is someone that can log into the site and can have multiple roles i.e.
# admin, leadOrg, organiser, leader, member
type User {
    username: String!
    role: String!
    firstName: String
    lastName: String
    name: String
    email: String
    phone: String
    createdAt: String
    lastLoggedIn: String
}
input userInput {
    username: String!
    email: String!
    role: String!
    firstName: String!
    lastName: String!
    phone: String
}
`
