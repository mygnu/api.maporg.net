export default `

type Viewer {
    # The current logged in user for the site default is 'anonymous'
    id: ID!
    username: String!
    role: String!
    firstName: String
    lastName: String
    name: String
    email: String
    phone: String
    lastLoggedIn: String
}
`
