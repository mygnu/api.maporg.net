export default `
type Worker {
    id: ID!
    firstName: String
    lastName: String
    name: String
    email: String
    phone: String
    role: String!
    joinDate: String
    updatedAt: String
    createdAt: String
}
`
