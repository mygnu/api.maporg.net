import express from 'express'
import { graphqlExpress, graphiqlExpress } from 'graphql-server-express'
import { makeExecutableSchema } from 'graphql-tools'
import bodyParser from 'body-parser'
import cookieSession from 'cookie-session'

import Schema from './schema'
import Resolvers from './resolvers'
import { createAnonymousToken, decodeToken, generateId } from './utils/crypto'
import { ANONYMOUS_TOKEN_DATA } from './utils/consts'
// import jwtMiddleware from './utils/jwt-middleware'
import models from './connectors/models'
const GRAPHQL_PORT = process.env.PORT || 9090

const graphQLServer = express()
    // if (process.env.NODE_ENV !== 'production') {
    // graphQLServer.use('*', cors())
    // }
const executableSchema = makeExecutableSchema({
    typeDefs: Schema,
    resolvers: Resolvers
})

function loadSessionData(req) {
    if (req.session && req.session.token) {
        return new Promise((resolve) => {
            let tokenData = null
            try {
                tokenData = decodeToken(req.session.token)
            } catch (err) {
                log(err)
            }
            resolve(tokenData)
        })
    }
    // else
    return new Promise((resolve) => {
        resolve(null)
    })
}

function getSessionData(req, res, next) {
    loadSessionData(req).then(tokenData => {
        if (!tokenData) {
            tokenData = ANONYMOUS_TOKEN_DATA
            req.session.token = createAnonymousToken()
            req.session.id = generateId()
        }

        console.log('Request From: ', tokenData.username, req.ip)
        req.tokenData = tokenData
        next()
    }).catch((err) => {
        log(err)
        res.sendStatus(400)
    })
}
graphQLServer.set('trust proxy', 1)
graphQLServer.use(cookieSession({
        name: 'session',
        // secureProxy: process.env.NODE_ENV === 'production',
        maxAge: 172800000, // two days
        keys: ['token', 'id']
    }))
graphQLServer.use(bodyParser.json())
graphQLServer.use(getSessionData)
    // graphQLServer.use(jwtMiddleware)

graphQLServer.use('/graphql',
    graphqlExpress(({ session, tokenData }) => ({ // get user and authorization from req object
        schema: executableSchema,
        context: { models, currentUser: tokenData, session },
        logger: { log: (e) => console.log(e) }
        // rootValue: 'Hello'
    })))

graphQLServer.use('/graphiql', graphiqlExpress({
    endpointURL: '/graphql'
}))

graphQLServer.listen(GRAPHQL_PORT, () => console.log(
    ` GraphQL Server is now running on http://localhost:${GRAPHQL_PORT}/graphql\n`,
    `GraphiQL debugger is now running on http://localhost:${GRAPHQL_PORT}/grapihql`
))
