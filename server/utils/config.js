require('dotenv').load()
export default {
    NODE_ENV: process.env.NODE_ENV,
    port: process.env.PORT,
    saltRounds: process.env.SALT_ROUNDS || 13,
    secret: process.env.TOKEN_SECRET ||
        'eyJhZG1pbiI6dHJ1ZSwibGVhZGVyIjp0cnVlLCJuYW1lIjoiSGFycnkgR2lsbCIsImNDYzNjUxMDI1LCJleHAiOjE0NjM2OTQyMj'
}
export const dbConfig = {
    'development': {
        'database': process.env.DB_DATABASE,
        'username': process.env.DB_USERNAME,
        'password': process.env.DB_PASSWORD,
        'host': process.env.DB_HOST,
        'port': process.env.DB_PORT,
        'dialect': process.env.DB_DIALECT
    },
    'test': {
        'database': process.env.DB_DATABASE,
        'username': process.env.DB_USERNAME,
        'password': process.env.DB_PASSWORD,
        'host': process.env.DB_HOST,
        'port': process.env.DB_PORT,
        'dialect': process.env.DB_DIALECT
    },
    'production': {
        'database': process.env.DB_DATABASE,
        'username': process.env.DB_USERNAME,
        'password': process.env.DB_PASSWORD,
        'host': process.env.DB_HOST,
        'port': process.env.DB_PORT,
        'dialect': process.env.DB_DIALECT
    }

}
