export const ROLES = {
    anonymous: 'anonymous',
    admin: 'admin',
    leadOrg: 'leadOrg',
    organiser: 'organiser',
    leader: 'leader',
    member: 'member'
}

// export const viewerId = 'ea7ae4b3-0f1d-42b4-96ea-9878f6e998d6'

export const ANONYMOUS_TOKEN_DATA = {
    id: 'ea7ae4b3-0f1d-42b4-96ea-9878f6e998d6',
    role: ROLES.anonymous,
    username: ROLES.anonymous
}
