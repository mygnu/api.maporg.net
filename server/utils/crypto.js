import bcrypt from 'bcrypt'
import config from './config'
import jwt from 'jsonwebtoken'
import { ANONYMOUS_TOKEN_DATA } from './consts'
import uuid from 'node-uuid'

export const generateId = () => uuid.v4()

export const hashPwd = (plain) => {
    return bcrypt.hashSync(plain, config.saltRounds)
}

export const checkPwd = (password, hash) => {
    return bcrypt.compareSync(password, hash)
}

export const createToken = (user) => {
    const options = {
        expiresIn: '2d',
        algorithm: 'HS256',
        issuer: 'maporg.net'
    }
    const { username, role, firstName, lastName, name, email } = user
    console.log('creating token for ' + username)
    const id = ANONYMOUS_TOKEN_DATA.id
    return jwt.sign({
        username,
        role,
        firstName,
        lastName,
        name,
        email,
        id
    }, config.secret, options)
}

export const createAnonymousToken = () => {
    return createToken(ANONYMOUS_TOKEN_DATA)
}

export const decodeToken = (token) => {
    let decoded = null
    if (!token) {
        return decoded
    }
    try {
        decoded = jwt.verify(token, config.secret)
    } catch (e) {
        console.log('cannot decode this token', e)
    }
    return decoded
}
