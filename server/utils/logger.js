import chalk from 'chalk'

export const log = (message) => {
    if (process.env.NODE_ENV !== 'test') {
        console.log(chalk.yellow(message))
    }
}
export const logError = (message) => {
    if (process.env.NODE_ENV !== 'test') {
        console.log(chalk.red.bold.strikethrough(message))
    }
}
export const logOk = (message) => {
    if (process.env.NODE_ENV !== 'test') {
        console.log(chalk.green(message))
    }
}
