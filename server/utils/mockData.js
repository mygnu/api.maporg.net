import { hashPwd, generateId } from './crypto'

export default (db) => {
    if (process.env.NODE_ENV === 'development') {
        db.User.create({
                username: 'harry',
                createdBy: 'harry',
                firstName: 'Harry',
                lastName: 'Gill',
                role: 'admin',
                email: 'test@test.com',
                password: hashPwd('password')
            })
            .then(function () {
                // console.log('Mock user created')
            }).catch((err) => { console.log(err.errors) })
        db.User.create({
                username: 'organiser',
                createdBy: 'harry',
                firstName: 'Martha',
                lastName: 'Gill',
                role: 'organiser',
                email: 'test1@test.com',
                password: hashPwd('password')
            })
            .then(function (created) {
                // console.log(created)
                // console.log('Mock user created')
            }).catch((err) => { console.log(err.errors) })

        db.Employer.create({
                id: generateId(),
                name: 'First Employer',
                state: 'NSW'
            })
            .then(function () {
                console.log('Employer Created')
            }).catch((err) => { console.log(err.errors) })
    }
}
