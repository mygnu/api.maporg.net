import { User } from '../server/connectors/models'
import { ROLES } from '../server/utils/consts'
import { hashPwd } from '../server/utils/crypto'
import chaiAsPromised from 'chai-as-promised'
import chai from 'chai'
chai.use(chaiAsPromised)
chai.should()
// const expect = chai.expect

describe('User Module', () => {
    it('User Module exists', () => {
        User.should.be.a('function')
    })
    const rawData = {
        username: 'sample',
        password: hashPwd('password'),
        firstName: 'Harry',
        lastName: 'Gill',
        email: 'test@test.com',
        createdBy: 'admin',
        role: ROLES.admin
    }
    // const user = new User(rawData)
    it('create user', () => {
        return User.create(rawData).then(data => data).should.eventually.be.ok
    })

    it('create user again', () => {
        return User.create(rawData).should.eventually.rejectedWith(Error)
    })

    it('get created user', () => {
        return User.findById(rawData.username).should.eventually.be.fullfilled
    })

    it('delete user', () => {
        return User.findById(rawData.username).then((user) => {
            user.destroy().then(result => result)
        }).should.eventually.be.false
    })

    it('Try get non existant user', () => {
        return User.findById(rawData.username).should.eventually.be.rejectedWith(Error)
    })
})
